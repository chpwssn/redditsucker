#!/usr/bin/python
import praw, os, re, time
from config import *

done_this_time = set()

if not os.path.isfile(downloadedIDsFile):
	open(downloadedIDsFile, 'a').close()

with open(downloadedIDsFile) as downloadedids:
    downloaded_already = downloadedids.read().splitlines()

r = praw.Reddit(user_agent="Reddit Sucker, a spin off of the OpenDirectoryBot")
if(botlogin):
    r.login(username,botpassword)
for subreddit in subreddits:
    if(fetchmode == "top"):
    	submissions = r.get_subreddit(subreddit).get_top(limit=getcount)
    else:
    	submissions = r.get_subreddit(subreddit).get_new(limit=getcount)
    for submission in submissions:
    	if submission.id not in done_this_time and submission.id not in downloaded_already:
    		done_this_time.add(submission.id)
    		sub = vars(submission)
    		if vars(submission)['is_self']:
    			print "Ignoring post by "+str(submission.author)+" it is a self post"
    			with open(downloadedIDsFile, "a") as downloadedids:
    				downloadedids.write(submission.id+'\n')
    		else:
    			suffix = str(sub['title'])+"/"+str(sub['url'])
    			suffix = suffix.replace("https://","")
    			suffix = suffix.replace("http://","")
    			suffix = suffix.replace("ftp://","")
    			print str(submission.author)+" submitted "+str(sub['url'])+" processing..."
    			safeTitle = re.sub(r'[^a-zA-Z0-9\[\]]',' ', sub['title'])
    			localdirectory = downloaddirectory+safeTitle
    			print safeTitle
    			if sub['ups'] < minimumUpvotes:
    				pass
    			with open(downloadedIDsFile, "a") as downloadedids:
    				downloadedids.write(submission.id+'\n')
    			if discriminateDownloads:
    				if nonsfw:
    					if sub['over_18']:
    						pass
    				if nosfw:
    					if not sub['over_18']:
    						pass
    			os.system("python "+scriptpath+"sucklink.py \""+localdirectory+"\" \""+str(sub['id'])+"\" \""+str(sub['url'])+"\" \""+suffix+"\" &")
