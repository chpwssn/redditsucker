#!/bin/bash


#functions here!
function ask {
    echo -n $1
    read -n 1 -r
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
            return 1;
    else
            exit
            echo "Aborting!"
    fi
}

#See if we already have git...
which git >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "Git Installed"
        echo "No action needed"
else
        needsgit=1
fi

#See if we already have python-pip...
which git >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "pip Installed"
        echo "No action needed"
else
        needspip=1
    fi

#See if we already have wget...
which wget >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "WGet Installed"
        echo "No action needed"
else
        needswget=1
fi

which apt >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "apt"
        if [ $needsgit -eq 1]
        then
                apt-get install git python-pip
        else
                apt-get install python-pip
        fi
        pip install praw
fi
which yum >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "yum"
        if [ $needswget -eq 1 ]
        then
                yum -y install wget
        fi
        #We need to install the noarch EPEL repos, let's check with the user
        ask "Hello, we detected you're using a noarch system. We need to add the EPEL repositories, continue? [y/N]:"
        pushd /tmp
        wget http://mirror-fpt-telecom.fpt.net/fedora/epel/6/i386/epel-release-6-8.noarch.rpm
        rpm -ivh epel-release-6-8.noarch.rpm
        popd 
        if [ $needspip -eq 1 ]
        then
                yum -y install python-pip
        else
                yum -y install python-pip
        fi
        pip install praw
fi
which pacman >/dev/null 2>&1
if [ $? -eq 0 ]
then
        echo "pacman"
        if [ $needsgit -eq 1]
        then
                pacman -S  git python-pip
        else
                pacman -S  python-pip
        fi
        pip install praw
fi

echo "RedditSucker  Copyright (C) 2014 Iceberg Technologies Limited with help from Meflakcannon"
echo "This program comes with ABSOLUTELY NO WARRANTY"
echo " "
echo "Done installing, copy example-config.py to config.py and update to your liking."
echo " "
echo "Once you're done, Run the script manually with python redditsucker.py or set a cron job!"